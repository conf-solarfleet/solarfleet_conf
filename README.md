# Config file for Solarfleet.

## controller
-> controleur de charge

## inverter
-> onduleur de PV

## meter
-> compteur électrique

## storage
-> Batterie

## terminal 
-> borne de recharge EV "simple"

## Wind
-> éolienne