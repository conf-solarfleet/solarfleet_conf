# Déclaration des compteurs


**device :**
* type
    * prod = conserne la production
    * grid = concerne l'échange avec le réseau
* slave_id = 1 si pas applicable

\
**Read :** 
|label|description|
|---|---|
|Power|puissance échangée avec le réseau [W]|
|PowerP1|consommation sur la phase 1 au général [W]|
|PowerP2|consommation sur la phase 2 au général [W]|
|PowerP3|consommation sur la phase 3 au général [W]|
|U1|Tension sur la phase 1|
|U2|Tension sur la phase 2|
|U3|Tension sur la phase 3|
|A1|Courant phase 1  !!! a multiplier par rapport TI|
|A2|Courant phase 2  !!! a multiplier par rapport TI|
|A3|Courant phase 3  !!! a multiplier par rapport TI|

**Type :** Précise le type des variables

**coef :** Coeficient de conversion si nécessaire
