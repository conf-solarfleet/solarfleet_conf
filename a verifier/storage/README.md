Config type : 

|**variable**|**Value**|**Desc**|**SI**|
|---|---|---|---|
**[device]**
|type|battery|
**[Read]**
|Power|registre|power|W|
|SOC|registre|SOC|taux|
**[Write]**
**[Type]**
|Power|type|
|SOC|type|
**[Coef]**
|Power|conversion coefficient|
|SOC|conversion coefficient|