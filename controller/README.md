#Déclaration des controlleurs
[device]
type = modèle du controlleur
-> controller : switch = address
-> gateway    : switch = unit_id

com = type de com
-> API
-> modbus

[Read]
register address list

[Write]
Read-write default
register address list

[Type]
variable type :
Float
DWord
Word

[Coef]
conversion coefficient to be in SI unit

[cpswitch]
a list for the different variable to be RW
address = a*i-a

#SPECIFICITE POWERDALE - magickey pour activer l'écriture modbus